/*
* Etude: 1 - HV Trees 
* Author: Tate Kennington
* Student ID: 5925152
* Date: 12/07
*/

#include<iostream>
#include<fstream>
//#include<vector>
#include<cmath>
#include<stack>
#include<getopt.h>
#include<stdlib.h>
#include<algorithm>

int WIDTH = 1024; //Image width in pixels
int HEIGHT = 1024; //Image height in pixels
int **image; //Image pixel buffer

/*
* Structure representing points to be drawn
*/
struct Point{
	int x; //X coordinate of the point
	int y; //Y coordinate of the point
	int length; //Length of the line to be drawn through the point
	int order; //Order of the point
	
	//Basic constructor
	Point(int _x, int _y, int _length, int _order){
		x = _x; y = _y; length = _length; order = _order;
	}
};

/*
* Draws an axis aligned straight line between two points in the image buffer
*/
void DrawLine(int srcx, int srcy, int destx, int desty){

	//Check the coordinates are valid
	if(srcx<0) srcx = 0;
	if(destx>=WIDTH) destx = WIDTH-1;
	if(srcy<0) srcy = 0;
	if(desty>=HEIGHT) desty = HEIGHT-1;

	//Draw the line
	if(srcx<destx){ //Line is parallel to x
		for(int x = srcx; x<=destx; x++){
			image[x][srcy] = 1;
		}
	}
	else if(srcy<desty){ //Line is parallel to y
		for(int y = srcy; y<=desty; y++){
			image[srcx][y] = 1;	
		}
	}
}

/*
* Writes the image buffer to a given file in PBM format
*/
void printOutput(std::string fname){
	std::ofstream output;
	output.open(fname.c_str());
	
	//Write header information
	output<<"P1"<<std::endl;
	output<<WIDTH<<" "<<HEIGHT<<std::endl;

	//Write pixel data
	for(int j = 0; j<HEIGHT; j++){
		output<<image[0][j];
		for(int i = 1; i<WIDTH; i++){
			output<<" "<<image[i][j];
		}	
		output<<std::endl;
	}
}

/*
* Draws the HV Tree recursively
* Memory: O(n)
*/
/*void DrawRecursive(int x, int y, int length, float ratio, int order, int max){

	//Base case
	if(order == max) return; 

	if(order%2==0){
		DrawLine(x-length/2, y, x+length/2, y);
		DrawRecursive(x-length/2,y,length*ratio,ratio, order+1, max);
		DrawRecursive(x+length/2,y, length*ratio, ratio, order+1, max);
	}
	else{
		DrawLine(x, y-length/2, x, y+length/2);
		DrawRecursive(x,y-length/2, ratio*length, ratio, order+1, max);
		DrawRecursive(x,y+length/2, length*ratio, ratio, order+1, max);
	}
}
*/

/*
* Draws the HV Tree iteratively in a breadth first fashion
* Memory: O(2^n)
*/
/*void DrawIterativeBFS(int length, float ratio, int order){
	std::vector<std::vector<std::pair<int, int> > > points = std::vector<std::vector<std::pair<int,int> > >(2);
	points[0].push_back(std::pair<int,int>(WIDTH/2,HEIGHT/2));
	for(int x = 0; x<order; x++){
		for(int i = 0; i<points[x%2].size(); i++){
			if(x%2==0){
				DrawLine(points[x%2][i].first-length/2, points[x%2][i].second, points[x%2][i].first+length/2, points[x%2][i].second);
				points[(x+1)%2].push_back(std::pair<int,int>(points[x%2][i].first-length/2,points[x%2][i].second));
				points[(x+1)%2].push_back(std::pair<int,int>(points[x%2][i].first+length/2,points[x%2][i].second));
			}
			else{
				 DrawLine(points[x%2][i].first, points[x%2][i].second-length/2, points[x%2][i].first, points[x%2][i].second+length/2);
				 points[(x+1)%2].push_back(std::pair<int,int>(points[x%2][i].first,points[x%2][i].second-length/2));
				 points[(x+1)%2].push_back(std::pair<int,int>(points[x%2][i].first,points[x%2][i].second+length/2));
			}

		}
		length*=ratio;
		std::cout<<points[x%2].size()<<std::endl;
		points[x%2].clear();
	}
}
*/

/*
* Draws the HV Tree iteratively in a depth first fashion
* Memory: O(n)
*/
void DrawIterativeDFS(int length, float ratio, int order){
	std::stack<Point> points;
	points.push(Point(WIDTH/2, HEIGHT/2, length, 0));
	while(!points.empty()){
		Point current = points.top();
		points.pop();

		if(current.order == order) continue;

		int x = current.x; int y = current.y; int length = current.length;

		if(length<1) continue;

		if(current.order%2==0){
			DrawLine(x-length/2, y, x+length/2, y);
			points.push(Point(x-length/2,y,length*ratio, current.order+1));
			points.push(Point(x+length/2,y,length*ratio, current.order+1));
		}
		else{
			DrawLine(x, y-length/2, x, y+length/2);
			points.push(Point(x,y-length/2,length*ratio, current.order+1));
			points.push(Point(x,y+length/2,length*ratio, current.order+1));

		}
		//std::cout<<points.size()<<std::endl;
	}
}

//Entry point
int main(int argc, char** argv){

	//If no options have been given show usage information
	if(argc == 1){
		std::cout<<"No options given, running program with default values"<<std::endl;
		std::cout<<"For usage information please run the program with the -h flag"<<std::endl;
	}



	
	int order = 1; //The order of tree to draw
	float ratio = 1; //The length ratio for successive orders
	std::string fileName = "hvtree.pbm"; //File to write results to

	//Parse command line options
	int c;
	while((c = getopt(argc, argv, "n:o:r:W:H:h")) != -1){
		switch(c){
			case 'o':
				fileName = optarg;
				break;
			case 'n':
				order = atoi(optarg);
				break;
			case 'r':
				ratio = atof(optarg);
				break;
			case 'W':
				WIDTH = atoi(optarg);
				break;
			case 'H':
				HEIGHT = atoi(optarg);
				break;
			case 'h':
				std::cout<<"Usage: ./hvtree [-n value] [-o filename] [-r ratio]"<<std::endl;
				std::cout<<"-n: specifies the order of tree, set to 1 by default"<<std::endl;
				std::cout<<"-o: specifies the file to write output to, set to hvtree.pbm by default"<<std::endl;
				std::cout<<"-r: specifies the ratio between subsequent lengths, set to 1 by default"<<std::endl;
				std::cout<<"-W: specifies the width of the output in pixels, set to 1024 by default"<<std::endl;
				std::cout<<"-H: specifies the height of the output in pixel, set to 1024 by default"<<std::endl;
				return 0;
			default:
				std::cout<<"Error parsing options"<<std::endl<<"Usage: ./hvtree [-n value] [-o filename] [-r ratio]"<<std::endl;
				std::cout<<"-n: specifies the order of tree, set to 1 by default"<<std::endl;
				std::cout<<"-o: specifies the file to write output to, set to hvtree.pbm by default"<<std::endl;
				std::cout<<"-r: specifies the ratio between subsequent lengths, set to 1 by default"<<std::endl;
				std::cout<<"-W: specifies the width of the output in pixels, set to 1024 by default"<<std::endl;
				std::cout<<"-H: specifies the height of the output in pixel, set to 1024 by default"<<std::endl;
				return -1;
		}
	}

	//Read in order and ratio
	//std::cin>>order>>ratio;

	//Initialize the image buffer
	image = (int**)malloc(WIDTH*sizeof(int*));
	for(int i = 0; i<WIDTH; i++){
		image[i] = (int*)malloc(HEIGHT*sizeof(int));
		for(int j = 0; j<HEIGHT; j++){
			image[i][j] = 0;
		}	
	}

	//Initial line length scaled to fit the image, calculated as the partial sum of a geometric series
	int lengthWidth = ratio==1?2*WIDTH/(order+1):(1-ratio*ratio)*WIDTH/(1-std::pow(ratio,order+1));
	int lengthHeight = ratio==1?2*HEIGHT/order:(1-ratio*ratio)*HEIGHT/(ratio*(1-std::pow(ratio,order)));
	int length = std::min(lengthWidth, lengthHeight);

	//DrawRecursive(WIDTH/2, HEIGHT/2, length, ratio, 0, order);
	//std::cout<<"================="<<std::endl;
	DrawIterativeDFS(length, ratio, order);
	//std::cout<<"================="<<std::endl;
	//DrawIterativeBFS(length, ratio, order);

	//Write image to file
	printOutput(fileName);

	return 0;
}
