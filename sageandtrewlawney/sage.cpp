#include<iostream>
#include<getopt.h>
#include<stdlib.h>
#include<ctime>

int main(int argc, char** argv){
	int memo[367][2];
	int gold[367];   gold[1] = 1;
    	for(int j = 1; j<367; j++){
        	for(int i = 1; i<367; i++){
        		if(j==1)memo[i][j] = 1;
			else{
				if(i==j) memo[i][j%2] = memo[i][(j-1)%2]+1;
				else if(j>i) memo[i][j%2] = gold[i];
				else if(i>j) memo[i][j%2] = memo[i-j][j%2] + memo[i][(j-1)%2];
    				while(memo[i][j%2]>12) memo[i][j%2]-=12;
				if(i==j) gold[i] = memo[i][j%2];
			}
        	}
   	}

	char c;
	time_t now = time(0);
	int day = localtime(&now)->tm_yday+1;

	if(argc == 1){
		std::cout<<"Usage Information:\n./sage [-h] [-t] [-a] [-d day]\n";
		std::cout<<"h: Displays usage information.\n";
		std::cout<<"t: Displays the Golden Hour for today, given by the systems local time.\n";
		std::cout<<"a: Displays the Golden Hour for every day.\n";
		std::cout<<"d: Displays the Golden Hour for a particular day from 1 to 366.\n";
		return -1;
	}

	while((c = getopt(argc, argv, "hatd:")) != -1){
		switch(c){
	
			case 'h':
				std::cout<<"Usage Information:\n./sage [-h] [-t] [-a] [-d day]\n";
				std::cout<<"h: Displays usage information.\n";
				std::cout<<"t: Displays the Golden Hour for today, given by the systems local time.\n";
				std::cout<<"a: Displays the Golden Hour for every day.\n";
				std::cout<<"d: Displays the Golden Hour for a particular day from 1 to 366.\n";
				break;
			case 'a':
				for(int i = 1; i<=366; i++){
				std::cout<<"The Golden Hour of Day "<<i<<" is "<<gold[i]<<"\n";
				}
				break;
			case 'd':
				if(atoi(optarg)<1 || atoi(optarg)>366){
					std::cout<<"ERROR: Invalid day\nDays must be in the range [1,366]\n";
					return -1;	
				}
				std::cout<<"The Golden Hour of Day "<<atoi(optarg)<<" is "<<gold[atoi(optarg)]<<"\n";
				break;
			case 't':
				std::cout<<"The Golden Hour of Today (Day "<<day<<") is "<<gold[day]<<"\n";
				break;
			default:
				std::cout<<"Usage Information:\n./sage [-h] [-t] [-a] [-d day]\n";
				std::cout<<"h: Displays usage information.\n";
				std::cout<<"t: Displays the Golden Hour for today, given by the systems local time.\n";
				std::cout<<"a: Displays the Golden Hour for every day.\n";
				std::cout<<"d: Displays the Golden Hour for a particular day from 1 to 366.\n";
				return -1;
		}
	}
	return 0;
}
