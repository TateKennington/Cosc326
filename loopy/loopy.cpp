#include<iostream>
#include<cstdlib>
#include<vector>
#include<algorithm>

const int NUM_MAX = 9000001;

int main(){
	int* divsum = (int*)malloc(sizeof(int)*NUM_MAX);
	int* loopy = (int*)malloc(sizeof(int)*NUM_MAX);
	for(int i = 1; i<NUM_MAX; i++){
		divsum[i] = 1;
		loopy[i] = -1;
	}
	for(int i = 2; i<NUM_MAX/2+1; i++){
		for(int j = 2*i; j<NUM_MAX; j+=i){
			divsum[j] += i;
		}
	}
	
	int currloop = 1;	

	for(int i = 1; i<NUM_MAX; i++){
		std::vector<int> path;
		int curr = i;
		
		if(divsum[i] == 1){
			loopy[i] = 0;
			continue;
		}
		if(loopy[i] != -1){
			continue;
		}
		while(std::find(path.begin(), path.end(), curr) == path.end()){
			path.push_back(curr);
			if(curr<NUM_MAX && loopy[curr] != -1) break;
			if(curr<NUM_MAX) curr = divsum[curr];
		}
		if(curr<NUM_MAX && loopy[curr] != -1){
			for(int j = 0; j<path.size(); j++){
				if(path[j]<NUM_MAX && loopy[path[j]]==-1) loopy[path[j]] = 0;
			}
		}
		else if(curr<NUM_MAX){
			int value = 0;
			for(int i = 0; i<path.size(); i++){
				if(path[i] == curr) value = currloop;
				if(path[i]<NUM_MAX)loopy[path[i]] = value;
			}
			currloop++;
		}
	}
	int loopcount[currloop];
	for(int i = 0; i<currloop; i++){
		loopcount[i] = 0;
	}
	for(int i = 1; i<NUM_MAX; i++){
		if(loopy[i] >= 1){
			std::cout<<loopy[i]<<" : "<<i<<std::endl;
			loopcount[loopy[i]]++;
		}
	}
	int max = 0;
	int loopid = -1;
	for(int i = 0; i<currloop; i++){
		if(loopcount[i]>max){
			max = loopcount[i];
			loopid = i;
		}
	}
	std::cout<<"Loops found : "<<currloop-1<<std::endl;
	std::cout<<"Longest loop is "<<loopid<<" with length "<<max<<std::endl;
	return 0;
}
