/*
 * Etude: 14 - Epidemic
 * Author: Tate Kennington
 * Student ID: 5925152
 * Date: 12/08/18
*/
#include<iostream>
#include<sstream>
#include<vector>
#include<getopt.h>

std::vector<std::vector<int> > grid; //Table for the state of the universe

/*
 * Determines if a cell at the given coordinates effects the final state of the universe 
*/
bool Redundant(std::vector<std::vector<int> > grid, int i, int j){
	std::vector<std::vector<int> > res = grid; //Copy of the state of the universe
	
	//Uninfect the given cell
	grid[i][j] = 0;

	bool change = true; /*Flag for if the universe has converged*/

	//Simulate both universes until they converge
	while(change){
		change = false;
	
		//Consider every cell
		for(int i = 0; i<grid.size(); i++){
			for(int j = 0; j<grid[i].size(); j++){
				int count = 0; /*Count of infected neighbours*/

				//Count the number of infect neighbours
				if(i-1>=0 && grid[i-1][j] == 1) count++;
				if(i+1<grid.size() && grid[i+1][j] == 1) count++;
				if(j-1>=0 && grid[i][j-1] == 1) count++;
				if(j+1<grid[i].size() && grid[i][j+1] == 1) count++;

				//If the cell has atleast 2 infected neighbours, infect it
				if(grid[i][j] == 0 && count>=2){
					grid[i][j] = 1;
					change = true;
				}
			
				count = 0;				

				//Count the number of infect neighbours
				if(i-1>=0 && res[i-1][j] == 1) count++;
				if(i+1<res.size() && res[i+1][j] == 1) count++;
				if(j-1>=0 && res[i][j-1] == 1) count++;
				if(j+1<res[i].size() && res[i][j+1] == 1) count++;

				//If the cell has atleast 2 infected neighbours, infect it
				if(res[i][j] == 0 && count>=2){
					res[i][j] = 1;
					change = true;
				}

			}
		}
	}

	//Compare both universes cell by cell, if they aren't identical
	//return false
	for(int i = 0; i<grid.size(); i++){
		for(int j = 0; j<grid[i].size(); j++){
			if(grid[i][j] != res[i][j]) return false;
		}
	}
	return true;
}

/*
 * Counts the number of infected cells in the universe currently
*/
int Count(){
	int res = 0;
	for(int i = 0; i<grid.size(); i++){
		for(int j = 0; j<grid[i].size(); j++){
			if(grid[i][j] == 1)res++;
		}
	}

	return res;
}

/*
 * Find the minimum number of cells that must be infected to infect the entire universe
 * And output one such configuration
*/
void FindOptimalConfiguration(){
	//For every row in the universe
	for(int i = 0; i<grid.size(); i++){

		//Completely infect that row
		for(int j = 0; j<grid[i].size(); j++){
			if(grid[i][j] == 0) grid[i][j] = 1;
		}
		
		//Continue if its the first row
		if(i == 0) continue;

		//Remove all redundant cells in the row above this one
		for(int j = 0; j<grid[i-1].size(); j++){
			if(Redundant(grid, i-1, j)) grid[i-1][j] = 0;
		}
	}
	
	//Remove redundant cells in the bottom row
	for(int j = 0; j<grid[grid.size()-1].size(); j++){
		if(Redundant(grid, grid.size()-1, j)) grid[grid.size()-1][j] = 0;
	}

	//Output the number of infected individuals
	std::cout<<Count()<<std::endl;

	//Print final state
	for(int i = 0; i<grid.size(); i++){
		for(int j = 0; j<grid[i].size(); j++){
			char c;
			if(grid[i][j] == 0) c = '.';
			if(grid[i][j] == 1) c = 'S';
			if(grid[i][j] == 2) c = 'I';
			std::cout<<c;
		}	
		std::cout<<std::endl;
	}

}

/*
 * Simulate fully the given universe configuration
*/
void SimulateUniverse(){
		bool change = true; /*Flag for if the universe has converged*/

		//While the universe still hasn't converged
		while(change){
			change = false;
	
			//Consider every cell
			for(int i = 0; i<grid.size(); i++){
				for(int j = 0; j<grid[i].size(); j++){
					int count = 0; /*Count of infected neighbours*/

					//Count the number of infect neighbours
					if(i-1>=0 && grid[i-1][j] == 1) count++;
					if(i+1<grid.size() && grid[i+1][j] == 1) count++;
					if(j-1>=0 && grid[i][j-1] == 1) count++;
					if(j+1<grid[i].size() && grid[i][j+1] == 1) count++;

					//If the cell has atleast 2 infected neighbours, infect it
					if(grid[i][j] == 0 && count>=2){
						grid[i][j] = 1;
						change = true;
					}
				}
			}
		}
		
		//Print final state
		for(int i = 0; i<grid.size(); i++){
			for(int j = 0; j<grid[i].size(); j++){
				char c;
				if(grid[i][j] == 0) c = '.';
				if(grid[i][j] == 1) c = 'S';
				if(grid[i][j] == 2) c = 'I';
				std::cout<<c;
			}	
			std::cout<<std::endl;
		}
}

/*
 * Program entry point
*/
int main(int argc, char** argv){
	bool sflag = false; /*Flag to simulate the universe*/
	bool oflag = false; /*Flag to find the optimal configuration*/
	char c; /*Command line flag*/
	
	//If the user hasn't given any flags
	if(argc == 1){
		std::cout<<"ERROR: Epidemic requires one flag to run"<<std::endl;
		std::cout<<"Usage Info: ./epidemic [-h] [-o] [-s]"<<std::endl;
		std::cout<<"o: Find the minimum number of cells to infect so that the entire universe is infected."<<std::endl;
	    std::cout<<"s: Simulate the given universe until it converges."<<std::endl;
		std::cout<<"h: Display usage information"<<std::endl;
		return -1;
	}

	//Get commandline flags
	while((c=getopt(argc, argv, "hso"))!= -1){
		switch(c){
			case 's':
				if(oflag){
					std::cout<<"ERROR: Epidemic cannot run with both -s and -o."<<std::endl;
					std::cout<<"Usage Info: ./epidemic [-h] [-o] [-s]"<<std::endl;
					std::cout<<"o: Find the minimum number of cells to infect so that the entire universe is infected."<<std::endl;
					std::cout<<"s: Simulate the given universe until it converges."<<std::endl;
					std::cout<<"h: Display usage information"<<std::endl;
					return -1;
				}
				sflag = true;
				break;
			case 'o':
				if(sflag){
					std::cout<<"ERROR: Epidemic cannot run with both -s and -o."<<std::endl;
					std::cout<<"Usage Info: ./epidemic [-h] [-o] [-s]"<<std::endl;
					std::cout<<"o: Find the minimum number of cells to infect so that the entire universe is infected."<<std::endl;
					std::cout<<"s: Simulate the given universe until it converges."<<std::endl;
					std::cout<<"h: Display usage information"<<std::endl;
					return -1;
				}
				oflag = true;
				break;
			case 'h':
				std::cout<<"Usage Info: ./epidemic [-h] [-o] [-s]"<<std::endl;
				std::cout<<"o: Find the minimum number of cells to infect so that the entire universe is infected."<<std::endl;
				std::cout<<"s: Simulate the given universe until it converges."<<std::endl;
				std::cout<<"h: Display usage information"<<std::endl;
				return 0;
			default:
				std::cout<<"ERROR: Parsing commandline arguments"<<std::endl;
				std::cout<<"Usage Info: ./epidemic [-h] [-o] [-s]"<<std::endl;
				std::cout<<"o: Find the minimum number of cells to infect so that the entire universe is infected."<<std::endl;
				std::cout<<"s: Simulate the given universe until it converges."<<std::endl;
				std::cout<<"h: Display usage information"<<std::endl;

				return -1;
		}
	}

	//Read until end of input
	while(!std::cin.eof()){

		//Initialize the state
		grid = std::vector<std::vector<int> >();

		//Read until an empty line
		std::string line;
		getline(std::cin, line);
		while(line.length()!=0){
			
			//Add a new row to the tables
			grid.push_back(std::vector<int>());

			std::stringstream ss(line);
			char temp;

			//Parse the line
			while(ss>>temp){
				switch(temp){
					case 'I':
						grid[grid.size()-1].push_back(2);
						break;
					case 'S':
						grid[grid.size()-1].push_back(1);
						break;
					case '.':
						grid[grid.size()-1].push_back(0);
						break;
				}
			}
			getline(std::cin, line);
		}

		if(sflag)SimulateUniverse();
		if(oflag)FindOptimalConfiguration();

	}
	return 0;
}
