/*
* Etude:  5 - Pokerhands
* Version: 1.1.0
* Date:   12/07/12
* Author: Tate Kennington
* Student ID: 5925152
*/

#include<iostream>
#include<vector>
#include<string>
#include<locale>
#include<algorithm>

//Forward declarations of states
void start(std::string text, int pos);
void suit(std::string text, int pos);
void leadingOne(std::string text, int pos);
void secondDigit(std::string text, int pos);
void numberCard(std::string text, int pos);
void letterCard(std::string text, int pos);

//Structure for card data
struct Card{
	int value;
	char suit;

	//Constructors
	Card(){
		value = -1;
		suit = 'n';
	}
	Card(int _value, char _suit){
		value = _value;
		suit = _suit;
	}

	/*
	* Returns a string representation of the card in standard form
	*/
	std::string ToString(){
		std::string res;

		if(value == 1 || value == 11 || value == 12 || value == 13){ //The card is a picture card
			if(value == 1) res+="A";
			if(value == 11) res+="J";
			if(value == 12) res+="Q";
			if(value == 13) res+="K";
		}
		else{ //The card is a number card
			res+=std::to_string(value);
		}
		res+=suit;
		return res;
	}
};

/*
* Compares two cards and determines which is greater or if they are equal
* Used for sorting
*/
bool CompareCard(const Card &a, const Card &b){
	//If the cards are aces then set the value to 14
	int avalue = a.value==1?14:a.value;
	int bvalue = b.value==1?14:b.value;
	
	if(avalue == bvalue){ //The cards have equal value
		if(a.suit == b.suit) return false;
		else return a.suit<b.suit;
	}
	else return avalue<bvalue;
}

char sep = 'a'; /*The first separator used, set to a when undefined*/
std::vector<Card> hand; /*List of parsed cards*/
Card current; /*The currrent card being parsed*/

/*
* State entered once the cards has been successfully parsed.
* Transitions to start on a valid separator
*/
void suit(std::string text, int pos){
	char curr = std::toupper(text[pos]);

	hand.push_back(current);
	if(hand.size()==5){ //Hand is full
		if(pos<text.length()){ //There is data left to parse
			std::cout<<"Invalid: "<<text<<std::endl;
		}
		else{ //We have completed parsing
			std::sort(hand.begin(), hand.end(), CompareCard);
			for(int i = 0; i<hand.size()-1; i++){
				if(hand[i].value == hand[i+1].value && hand[i].suit == hand[i+1].suit){
					std::cout<<"Invalid: "<<text<<std::endl;
					return;
				}
			}
			std::cout<<hand[0].ToString();
			for(int i = 1; i<hand.size(); i++){
				std::cout<<" "<<hand[i].ToString();
			}
			std::cout<<std::endl;
		}
	}
	else if(sep=='a' && (curr == ' ' || curr == '/' || curr == '-')){//This is the first valid separator
		sep = curr;
		start(text, pos+1);
	}
	else if(curr == sep){ //The separator matches the previous separators
		start(text, pos+1);
	}
	else{ //The separator is invalid
		std::cout<<"Invalid: "<<text<<std::endl;
	}
}

/*
* State entered after parsing a valid two digit value
* Transitions to suit after parsing a valid suit
*/
void secondDigit(std::string text, int pos){
	char curr = std::toupper(text[pos]);

	if(curr == 'C' || curr == 'D' || curr == 'H' || curr == 'S'){ //We recognize the suit
		current.suit = curr;
		suit(text, pos+1);
	}
	else{ //The character is invalid
		std::cout<<"Invalid: "<<text<<std::endl;
	}
}

/*
* State entered after parsing a one
* Transitions to suit after parsing a valid suit
* Transitions to secondDigit after parsing a second digit
*/
void leadingOne(std::string text, int pos){
	char curr = std::toupper(text[pos]);

	if(curr == 'C' || curr == 'D' || curr == 'H' || curr == 'S'){ //The character is a valid suit
		current.value = 1;
		current.suit = curr;
		suit(text, pos+1);
	}
	else if(curr>='0' && curr<='3'){ //The character is a valid second digit
		current.value = 10 + curr - '0';
		secondDigit(text, pos+1);
	}
	else{ //The character is invalid
		std::cout<<"Invalid: "<<text<<std::endl;
	}
}

/*
* State entered after parsing a single digit card value
* Transitions to suit after parsing a valid suit
*/
void numberCard(std::string text, int pos){
	char curr = std::toupper(text[pos]);

	if(curr == 'C' || curr == 'D' || curr == 'H' || curr == 'S'){ //The character is a valid suit
		current.suit = curr;
		suit(text, pos+1);
	}
	else{ //The character is invalid
		std::cout<<"Invalid:"<<text<<std::endl;
	}
}

/*
* State entered after parsing a single letter card value
* Transitions to suit after parsing a valid suit
*/
void letterCard(std::string text, int pos){
	char curr = std::toupper(text[pos]);

	if(curr == 'C' || curr == 'D' || curr == 'H' || curr == 'S'){ //The character is a valid suit
		current.suit = curr;
		suit(text, pos+1);
	}
	else{ //The character is invalid
		std::cout<<"Invalid: "<<text<<std::endl;
	}
}

/*
* Start State
* Entered at the beginning of every card
* Transitions to leadingOne on parsing a one
* Transitions to numberCard on parsing a single digit card value
* Transitions to letterCard on parsing a single letter card value
*/
void start(std::string text, int pos){
	current = Card();
	char curr = std::toupper(text[pos]);

	if(curr=='1'){ //The first character is a one
		leadingOne(text, pos+1);
	}
	else if(curr>='2' && curr<='9'){ //The first character is a single digit
		current.value = curr - '0';
		numberCard(text, pos+1);	
	}
	else if(curr == 'A' || curr == 'T' || curr == 'J' || curr == 'Q' || curr == 'K'){ //The first character is a letter
		if(curr == 'A') current.value = 1;
		if(curr == 'T') current.value = 10;
		if(curr == 'J') current.value = 11;
		if(curr == 'Q') current.value = 12;
		if(curr == 'K') current.value = 13;
		letterCard(text, pos+1);
	}
	else{ //The character is invalid
		std::cout<<"Invalid: "<<text<<std::endl;
	}
	
}

//Program entry point
int main(){
	std::string input;
	std::getline(std::cin,input);
	while(!std::cin.eof()){
		sep='a';
		hand = std::vector<Card>();
		start(input, 0);
		std::getline(std::cin, input);
	}
	return 0;
}
