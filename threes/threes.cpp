/*
* Etude: 13 - Threes
* Author: Tate Kennington
* Student ID: 5925152
* Date: 20/07
*/

#include<iostream>
#include<cmath>
#include<getopt.h>

/*
* Checks if two numbers are coprimes
*/
bool areCoprime(int a, int b){

    //Ensure that a>b
    if(b>a) return areCoprime(b,a);

    //If they're both even return false
    if(!((a|b)&1)) return false;

    //Calculate the GCD of the numbers
    while(b){
        int temp = a%b;
        a = b;
        b = temp;
    }

    //If the GCD is 1 they're coprime
    return a == 1;
}

/*
* Checks if three numbers are coprimes pairwise
*/
bool arePairwiseCoprime(int x, int y, int z){

    //Return false if any pair aren't coprime
    if(!areCoprime(x,y)) return false;
    if(!areCoprime(y,z)) return false;
    if(!areCoprime(x,z)) return false;
    return true;
}

/*
* List satisfactory triples in order of increasing x
*/
void listByX(){
    int c = 0; /*Counter for the number of triples we've found*/
    for(int x = 1; c<70; x++){// Fix some positive integer x
            for(int a = x; c<70 && a>0; a--){//Fix some a no greater than x

                //Calculate z and y from x and a
                int z = sqrt((x*x+a*a-1)/(2*a));
                int y = (x*x-a*a-1)/(2*a);

                //If x,y,z are a valid triple print them and increment c
                if(y>x && x>z && x*x+y*y == z*z*z*z+1 && arePairwiseCoprime(x,y,z)){
                    std::cout<<c+1<<" "<<x<<" "<<y<<" "<<z<<"\n";
                    c++;
                }
            }
    }
}

/*
* List satisfactory triples in order of increasing z
*/
void listByZ(){
    int c = 0; /*Counter for the number of triples we've found*/
    for(int z = 1; c<70; z++){//Fix some positive integer z
        int r2 = z*z*z*z+1; /*The value of the RHS of the expression*/
        for(int a = 0; a<=z*z-z-2; a++){//Fix some integer a no greater than z^2-z-2

            //Calculate y using z and a, and x using y and a
            int y = z*z-a;
            int x = sqrt(2*a*y+a*a+1);

            if(x<0) break;//Break if x is not positive

            //If x,y,z are a valid triple, print them and increment c
            if(y>x && x>z && x*x+y*y == r2 && arePairwiseCoprime(x,y,z)){
                    std::cout<<c+1<<" "<<x<<" "<<y<<" "<<z<<"\n";
                    c++;
            }
        }
    }
}

/*
* Program entry point
*/
int main(int argc, char** argv){

    //The user hasn't given any options, show usage information
    if(argc == 1){
        std::cout<<"ERROR: This program requires exactly one option.\n";
        std::cout<<"Usage Information:\n./threes [-x] [-z] [-b] [-h]\n";
        std::cout<<"x: Display the first 70 triples in increasing order of x.\n";
        std::cout<<"z: Display the first 70 triples in increasing order of z.\n";
        std::cout<<"b: Display the first 70 triples for both increasing x and z.\n";
        std::cout<<"h: Display usage information.\n";
        return 0;
    }

    char c;
    while((c = getopt(argc, argv, "xzbh")) != -1){
        switch(c){
            case 'x':
                listByX();
                return 0;
            case 'z':
                listByZ();
                return 0;
            case 'b':
                listByX();
                std::cout<<"\n";
                listByZ();
                return 0;
            case 'h':
                std::cout<<"Usage Information:\n./threes [-x] [-z] [-b] [-h]\n";
                std::cout<<"x: Display the first 70 triples in increasing order of x.\n";
                std::cout<<"z: Display the first 70 triples in increasing order of z.\n";
                std::cout<<"b: Display the first 70 triples for both increasing x and z.\n";
                std::cout<<"h: Display usage information.\n";
                break;
            default:
                std::cout<<"Usage Information:\n./threes [-x] [-z] [-b] [-h]\n";
                std::cout<<"x: Display the first 70 triples in increasing order of x.\n";
                std::cout<<"z: Display the first 70 triples in increasing order of z.\n";
                std::cout<<"b: Display the first 70 triples for both increasing x and z.\n";
                std::cout<<"h: Display usage information.\n";
                return -1;
        }
    }
    return 0;
}
